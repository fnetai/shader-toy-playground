void mainImage(out vec4 fragColor,in vec2 fragCoord)
{
    vec3 rColor=vec3(.9,0.,.3);
    vec3 gColor=vec3(0.,.9,.3);
    vec3 bColor=vec3(0.,.3,.9);
    vec3 yColor=vec3(.9,.9,.3);
    
    vec2 p=(fragCoord.xy*2.-iResolution.xy);
    p/=min(iResolution.x,iResolution.y);
    
    float a=sin(p.y*1.5-iTime*.1)/1.;
    float b=cos(p.y*1.5-iTime*.2)/1.;
    float c=sin(p.y*1.5-iTime*.3+3.14)/1.;
    float d=cos(p.y*1.5-iTime*.5+3.14)/1.;
    
    float e=.1/abs(p.x+a);
    float f=.1/abs(p.x+b);
    float g=.1/abs(p.x+c);
    float h=.1/abs(p.x+d);
    
    vec3 destColor=rColor*e+gColor*f+bColor*g+yColor*h;
    fragColor=vec4(destColor,1.);
}