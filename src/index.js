import ShaderToy from "@fnet/shader-toy";

// // https://cineshader.com/view/XsBfRW
// // https://www.shadertoy.com/view/XsBfRW
// import fshader from "./pretty-hip/fshader.glsl";

// // https://www.shadertoy.com/view/MlKSWm
// import fshader from "./sparks-drifting/fshader.glsl";

// // https://www.shadertoy.com/view/MlKSWm
// import fshader from "./ebru/fshader.glsl";


// https://www.shadertoy.com/view/mdKBWd
import fshader from "./rainbow/fshader.glsl";

// import fshader from "./test/fshader.glsl";

export default async (args) => {
    await ShaderToy({ ...args, fshader, canvas: { backgroundColor: "#000" }, stats: true, mouse: true })
}