vec3 palette(float t,vec3 a,vec3 b,vec3 c,vec3 d){
    return a+b*cos(6.28318*(c*t+d));
}

// void mainImage(out vec4 fragColor,in vec2 fragCoord){
    
    //     // // step-1
    //     // fragColor=vec4(0.,0.,0.,1.);
    
    //     // // step-2
    //     // vec2 uv=fragCoord/iResolution.xy;
    //     // fragColor=vec4(uv.x,0.,0.,1.);
    
    //     // // step-3
    //     // vec2 uv=fragCoord/iResolution.xy;
    //     // fragColor=vec4(uv,0.,1.);
    
    //     // // step-3
    //     // vec2 uv=fragCoord/iResolution.xy*2.-1.;
    //     // fragColor=vec4(uv,0.,1.);
    
    //     // // step-4
    //     // vec2 uv=fragCoord/iResolution.xy*2.-1.;
    //     // float d=length(uv);
    //     // // fragColor=vec4(d,0.,0.,1.);
    //     // fragColor=vec4(d,d,d,1.);
    
    //     // Decide which dimension to use based on aspect ratio
    //     float aspectRatio=iResolution.x/iResolution.y;
    //     // "contain" mode
    //     float divisor=(aspectRatio<=1.)?iResolution.x:iResolution.y;
    
    //     // // step-4
    //     // vec2 uv=(fragCoord*2.-iResolution.xy)/divisor;
    //     // float d=length(uv);
    //     // d-=.5;
    //     // d=abs(d);
    //     // // d=step(0.1,d);
    //     // d=smoothstep(0.,.01,d);
    //     // fragColor=vec4(d,d,d,1.);
    
    //     // float angle_in_degrees = 30.0;
    //     // float angle_in_radians = angle_in_degrees * 3.14159 / 180.0;
    //     // float result = sin(angle_in_radians);
    
    //     // // step-5
    //     // vec2 uv=(fragCoord*2.-iResolution.xy)/divisor;
    //     // float d=length(uv);
    //     // vec3 col=palette(d+iTime,vec3(.5,.5,.5),vec3(.5,.5,.5),vec3(1.,1.,1.),vec3(.263,.416,.557));
    
    //     // d=sin(d*8.+iTime)/8.;
    //     // d=abs(d);
    //     // // d=step(0.1,d);
    //     // // d=smoothstep(0.,.1,d);
    
    //     // // vec3 col=vec3(1.,2.,3.);
    
    //     // d=0.02/d;
    
    //     // col*=d;
    
    //     // fragColor=vec4(col,1.);
    
    //     // step-6
    //     // vec2 uv=(fragCoord*2.-iResolution.xy)/divisor;
    
    //     // vec2 uv0=uv;
    
    //     // uv*=2.;
    //     // uv=fract(uv*2.)-.5;
    //     // uv=fract(fract(uv)*sin(uv));
    
    //     // float d=length(uv);
    //     // vec3 col=palette(length(uv0)+iTime,vec3(.5,.5,.5),vec3(.5,.5,.5),vec3(1.,1.,1.),vec3(.263,.416,.557));
    
    //     // d=sin(d*2.+iTime)/2.;
    //     // d=abs(d);
    //     // d=step(0.1,d);
    //     // d=smoothstep(0.,.1,d);
    
    //     // vec3 col=vec3(1.,2.,3.);
    
    //     // d=.02/d;
    
    //     // col*=d;
    
    //     // fragColor=vec4(col,1.);
    
    //     // vec2 uv=(fragCoord*2.-iResolution.xy)/divisor;
    
    //     // // Circle center and radius
    //     // vec2 center = vec2(0.0, 0.0);
    //     // float radius = 0.4;
    
    //     // // Signed Distance Function for the circle
    //     // float d = length(uv - center) - radius;
    
    //     // // Coloring based on the SDF
    //     // vec3 col;
    //     // if (d < 0.0) {
        //     //     col = vec3(0.0, 1.0, 0.0);  // Inside circle color (Green)
    //     // } else if (d < 0.01) {
        //     //     col = vec3(1.0, 1.0, 1.0);  // Border color (White)
    //     // } else {
        //     //     col = vec3(0.0, 0.0, 1.0);  // Outside circle color (Blue)
    //     // }
    
    //     // // Output color
    //     // fragColor = vec4(col, 1.0);
// }

// float sdCircle( in vec2 p, in float r )
// {
    //     return length(p)-r;
// }

// // b.x = width
// // b.y = height
// // r.x = roundness top-right
// // r.y = roundness boottom-right
// // r.z = roundness top-left
// // r.w = roundness bottom-left
// float sdRoundBox( in vec2 p, in vec2 b, in vec4 r )
// {
    //     r.xy = (p.x>0.0)?r.xy : r.zw;
    //     r.x  = (p.y>0.0)?r.x  : r.y;
    //     vec2 q = abs(p)-b+r.x;
    //     return min(max(q.x,q.y),0.0) + length(max(q,0.0)) - r.x;
// }

// void mainImage( out vec4 fragColor, in vec2 fragCoord )
// {
    // 	vec2 p = (2.0*fragCoord-iResolution.xy)/iResolution.y;
    //     vec2 m = (2.0*iMouse.xy-iResolution.xy)/iResolution.y;
    
    // 	vec2 si = vec2(0.9,0.6) + 0.3*cos(iTime+vec2(0,2));
    //     vec4 ra = 0.3 + 0.3*cos( 2.0*iTime + vec4(0,1,2,3) );
    //     ra = min(ra,min(si.x,si.y));
    
    // 	float d = sdRoundBox( p, si, ra );
    
    //     vec3 col = (d>0.0) ? vec3(0.9,0.6,0.3) : vec3(0.65,0.85,1.0);
    // 	col *= 1.0 - exp(-6.0*abs(d));
    // 	col *= 0.8 + 0.2*cos(150.0*d);
    // 	col = mix( col, vec3(1.0), 1.0-smoothstep(0.0,0.01,abs(d)) );
    
    //     if( iMouse.z>0.001 )
    //     {
        //     d = sdRoundBox(m, si, ra );
        //     col = mix(col, vec3(1.0,1.0,0.0), 1.0-smoothstep(0.0, 0.005, abs(length(p-m)-abs(d))-0.0025));
        //     col = mix(col, vec3(1.0,1.0,0.0), 1.0-smoothstep(0.0, 0.005, length(p-m)-0.015));
    //     }
    
    // 	fragColor = vec4(col,1.0);
// }

// #define rot(r) mat2(cos(r), sin(r), -sin(r), cos(r))
// #define ROTATE p.xy *= rot(t*.2); p.xz *= rot(t*.3);
// #define SCREEN vec3 r = iResolution, p;  u = (u - r.xy / 2.) / r.y; o *= 0.;
// #define LOOP(a) int j = 0; while(j++ < a)

// void mainImage(out vec4 o, vec2 u){
    
    //     SCREEN
    
    //     float i = 0., s, a, d, t = iTime;
    
    //     while( i++ < 99.){
        //         p = vec3(a * u + .5, a - 2.);
        
        //         ROTATE
        
        //         s = 3.;
        //         LOOP(10)
        //             d = 1. / min(dot(p, p), .9),
        //             s *= d,
        //             p = abs(p) * d - vec3(1, 2.5,.7);
        
        //         d = abs(length(p.xz) / s - .03) + 1e-3;
        
        //         a += d * .6;
        
        //         o.rgb += .015 / exp(vec3(200, 160, 150) * d);
        
        //         d < .002
        //             ? o += .8 / i
        //             : o;
    //     }
// }

// #define R iResolution.xy
// #define T iTime*.5
// #define rot(a)mat2(cos(a),sin(a),-sin(a),cos(a))
// #define pi atan(.0,-1.)
// #define rayMarchMacro float i,s,t;while(i++<2e2){s=M(t*vec3(uv,-1));t+=s*.1;if(t>100.||s<.02)break;}

// float M(vec3 p){
    
    //         // cachaça
    //         float umaDose=.01;
    //         p.xy*=rot(umaDose*dot(vec2(cos(T),sin(T)),p.yx));
    
    //         // dirige você, então!
    
    //         p-=mix(
        //                 vec3(
            //                         sin(T)*p.z-cos(3.*T)*1.5-.5,
            //                         cos(T)*p.z-sin(2.*T)*1.5-.5,
            //                         T*.96+.5
        //                 )
        //                 ,
        //                 vec3(
            //                         sin(3.*T)*p.z-cos(T)*1.5-.5,
            //                         cos(2.*T)*p.z-sin(T)*1.5-.5,
            //                         T*.96+.5
        //                 )
        //                 ,
        //                 cos(T*.1)
        
    //         );
    
    //         // grid sdf
    //         vec3 a=(mod(p,2.)-1.)*3.;
    //         vec3 b=abs(mod(p+.05,.1)-.05)*2.;
    
    //         vec2 q=vec2(
        //                 min(
            //                         length(a.xy),
            //                         min(
                //                                 length(a.yz),
                //                                 length(a.xz)
            //                         )
        //                 )-.8,
        
        //                 min(b.x,min(b.y,b.z))
    //         );
    
    //         return length(q)-.01;
// }

// void mainImage(out vec4 O,vec2 u){
    //         vec2 uv=(u+u-R)/R.y;
    
    //         rayMarchMacro
    
    //         O=vec4(t*vec3(6,7,9)*.02,1);
// }

float f1(float x,float r){
    return sqrt(r*r-x*x);
}

float f2(float x,float a){
    return a*sqrt(pow(x,2.)*(1.-pow(x,2.)));
}

#define TRANSLATE2D(p, t) (p + t)
#define SCALE2D(p, s) (p * s)
#define ROTATE2D(p, angle) mat2(cos(angle), -sin(angle), sin(angle), cos(angle)) * p
#define SKEWX(p, alpha) mat2(1.0, alpha, 0.0, 1.0) * p
#define SKEWY(p, alpha) mat2(1.0, 0.0, alpha, 1.0) * p

void mainImage(out vec4 fragColor,in vec2 fragCoord){
    
    float aspectRatio=iResolution.x/iResolution.y;
    
    float divisor=(aspectRatio<=1.)?iResolution.x:iResolution.y;
    
    vec2 uv=(fragCoord*2.-iResolution.xy)/divisor;

    // Rotate
    // vec2 uv1_rotated=ROTATE2D(uv, 0.);
    // uv1_rotated=TRANSLATE2D(uv1_rotated, vec2(0.5, 0.5));
    // uv1_rotated=SCALE2D(uv1_rotated, vec2(0.5, 0.5));
    // uv1_rotated=SKEWX(uv1_rotated,iTime);
    // uv1_rotated=SKEWY(uv1_rotated,iTime);
    // uv1_rotated=TRANSLATE2D(uv1_rotated, vec2(0.5, 0.5));
    
    // float theta = ...; // Angle in radians

    // vec2 uv1_rotated;
    // uv1_rotated.x = uv1.x * cos(theta) - uv1.y * sin(theta);
    // uv1_rotated.y = uv1.x * sin(theta) + uv1.y * cos(theta);

    // vec2 uv1=vec2(f1(uv1_rotated.x,sin(iTime)),f1(uv1_rotated.y,sin(iTime)));
    vec2 uv1=vec2(f2(uv.x,2.),f2(uv.y,1.));
    
    // float d=length(uv1);
    
    vec3 col=vec3(abs(uv1.x),0.,0.);
    
    fragColor=vec4(col,1.);
}